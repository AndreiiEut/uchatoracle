#include "uchat.h"

static int mx_strlen(const char *s) {
    int count = 0;
    while (s[count]) count++;
    return count;
}

static void mx_printstr(const char *s) { write(1, s, mx_strlen(s)); }

int main() {
    mx_printstr("original repo: https://gitlab.com/hewstonfox/UChat");
}
