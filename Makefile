all:
	$(MAKE) -C client
	$(MAKE) -C server

clean:
	$(MAKE) -C client clean
	$(MAKE) -C server clean

uninstall: clean
	$(MAKE) -C client uninstall
	$(MAKE) -C server uninstall

deploy: $(FW_PATHS)
	./scripts/deploy.sh


reinstall: uninstall all

