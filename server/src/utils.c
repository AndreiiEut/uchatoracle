#include "server.h"

void mx_printstr(const char *s) { write(1, s, mx_strlen(s)); }

void mx_printchar(char c) { write(1, &c, 1); }

void mx_printint(int n) {
    if (n == -2147483647 - 1) {
        write(1, "-2147483648", 11);
        return;
    }
    if (n < 0) {
        mx_printchar('-');
        n *= -1;
    }
    if (n < 10) {
        mx_printchar(n + '0');
    } else {
        mx_printint(n / 10);
        mx_printchar(n % 10 + '0');
    }
}

bool mx_streq(const char *s1, const char *s2) { return !mx_strcmp(s1, s2); }

int mx_strcmp(const char *s1, const char *s2) {
    while (*s1 && *s1 == *s2) {
        s1++;
        s2++;
    }
    return *s1 - *s2;
}

int mx_strlen(const char *s) {
    int count = 0;
    while (s[count]) count++;
    return count;
}
