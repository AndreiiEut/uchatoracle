#pragma once 

#include <fcntl.h>
#include <locale.h>
#include <stdbool.h>
#include <stdio.h>
#include <limits.h>
#include <stdlib.h>
#include <unistd.h>
#include <wchar.h>
#include <libgen.h>

void mx_printstr(const char *s);

void mx_printchar(char c);

void mx_printint(int n);

bool mx_streq(const char *s1, const char *s2);

int mx_strcmp(const char *s1, const char *s2);

int mx_strlen(const char *s);
