#include "server.h"

static void demonize() {
    pid_t pid = fork();
    if (pid < 0) {
        perror("Cild process creation failed");
        exit(1);
    }
    if (pid > 0) {
        printf("Deamon started\nUse 'kill %d' to stop.\n", pid);
        exit(0);
    }
    signal(SIGCHLD, SIG_IGN);
    signal(SIGHUP, SIG_IGN);
}


int main(int argc, const char **argv) {
    int port = 9000;
    short demon = true;
    if (argc >= 2 && !mx_streq(argv[1], "-")) {
        int new_port = atoi(argv[1]);
        if (new_port > 0)
            port = new_port;
        else {
            mx_printstr("Invalid Port \"");
            mx_printstr(argv[1]);
            mx_printstr("\".\n");
        }
    }
    if (argc >= 3 && mx_streq(argv[2], "--no-demon"))
        demon = false;

    if (demon)
        demonize();

    mx_printstr("Starting at port: ");
    mx_printint(port);
    mx_printchar('\n');

    while (1) {}
    
    return 0;
}
